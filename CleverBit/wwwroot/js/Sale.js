﻿$(".buyNowBtn").click(function () {

    $(".buyNowBtn").prop('disabled', true);

    var dataSend = {
        ProductId: $("#productId").val(),
        MockDb: $("#mockDb").val()
    };

    var payload = JSON.stringify(dataSend);
    $.ajax({
        url: "../../api/Sales/saleItem",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: payload,
        success: function (response) {
            console.log(response);
            $("#resultRequest").text("You have just bought this fantastic product");
            if (response.numberSales >= 5) {
              
                $("#bestSaleContainer").html("<h2>This is a POPULAR PRODUCT</h2><img src='../img/mostPopular.jpg'/>");
                $("#bestsell").addClass("imgBest");
            }
            $(".buyNowBtn").prop('disabled', false);
        },
        error: function (response) {
            console.log(response);
            $("#resultRequest").text("Please retry, as we could not process your order, if presistes please contact us");
            $(".buyNowBtn").prop('disabled', false);
        }

    });
});
﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CleverBit.Models.Interfaces
{
    public interface IDataModel
    {

        public int Id { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? UpdateDate { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleverBit.Models.Interfaces
{
    public interface IRepository<T> where T : IDataModel

    {

        T GetById(int id);

        List<T> GetAll();

        T Create(T entity);

        void Delete(T entity);

        T Update(T entity);

    }
}

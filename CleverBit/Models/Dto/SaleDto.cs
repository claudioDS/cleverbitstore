﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleverBit.Models.Dto
{
    public class SaleDto
    {
        public int ProductId { get; set; }

        public bool MockDb { get; set; }
    }
}

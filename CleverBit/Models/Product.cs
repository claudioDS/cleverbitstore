﻿using CleverBit.Models.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CleverBit.Models
{
    public class Product : IDataModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public int NumberSales { get; set; }

        public string Description { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CleverBit.Data.MockDb;
using CleverBit.Models;
using Microsoft.AspNetCore.Mvc;

namespace CleverBit.Controllers
{
    public class ProductMockController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ViewProduct()
        {
            Product model = AppMockDbContext.Instance.GetAll().First();
            return View(model);
        }
    }
}
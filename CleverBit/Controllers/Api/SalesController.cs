﻿using CleverBit.Data;
using CleverBit.Data.MockDb;
using CleverBit.Models;
using CleverBit.Models.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CleverBit.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesController : ControllerBase
    {

        private readonly ApplicationDbContext _context;

        public SalesController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Authorize]
        [HttpPost("saleItem")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult SaleItem([FromBody]SaleDto detail)
        {
            if (detail == null || detail.ProductId < 1)
            {
                return BadRequest();
            }

            if (detail.MockDb)
            {
                Product product = AppMockDbContext.Instance.GetById(detail.ProductId);

                if (product == null)
                {
                    return BadRequest(); // or could be not found
                }

                product.NumberSales += 1;

                product = AppMockDbContext.Instance.Update(product);

                return Ok(new { product.NumberSales });
            }
            else
            {
                Product efProduct = _context.Products.Find(detail.ProductId);

                if (efProduct== null)
                {
                    return BadRequest();
                }

                efProduct.NumberSales += 1;

                _context.Entry(efProduct).State = Microsoft.EntityFrameworkCore.EntityState.Modified;

                _context.SaveChanges();

                return Ok(new { efProduct.NumberSales });
            }
        }
    }
}
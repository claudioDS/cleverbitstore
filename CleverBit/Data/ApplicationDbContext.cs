﻿using CleverBit.Models;
using CleverBit.Models.Interfaces;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CleverBit.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public override int SaveChanges()
        {
            DateTime tstamp = DateTime.UtcNow;

            var addedEntities = this.ChangeTracker.Entries()
                                          .Where(e => e.State == EntityState.Added)
                                          .Select(e => e.Entity).OfType<IDataModel>();

            var updatedEntities = this.ChangeTracker.Entries()
                                          .Where(e => e.State == EntityState.Modified)
                                          .Select(e => e.Entity).OfType<IDataModel>();

            foreach (var timeStamped in addedEntities)
            {
                timeStamped.CreationDate = DateTime.Now;
            }

            foreach (var timeS in updatedEntities)
            {
                timeS.UpdateDate = DateTime.Now;
            }

            return base.SaveChanges();

        }

        public DbSet<Product> Products { get; set; }
    }

}

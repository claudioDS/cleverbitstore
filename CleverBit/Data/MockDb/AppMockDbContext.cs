﻿using CleverBit.Models;
using CleverBit.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CleverBit.Data.MockDb
{
    public class AppMockDbContext : IRepository<Product>
    {
        private static readonly Lazy<AppMockDbContext> lazyObj = new Lazy<AppMockDbContext>(() => new AppMockDbContext());
        private static List<Product> Repo { get; set; }

        public static AppMockDbContext Instance
        {
            get
            {
                return lazyObj.Value;
            }
        }

        private AppMockDbContext()
        {
            if (Repo == null)
            {
                Repo = new List<Product>()
            {
                new Product()
                {
                    Id  = 1,
                    CreationDate = DateTime.Now,
                    Description = "First item of this store",
                    Name = "Amazing Jedi Sword",
                    NumberSales = 0,
                    Price = 100,
                    UpdateDate = null
                },
                new Product()
                {
                    Id  = 2,
                    CreationDate = DateTime.Now.AddDays(-1),
                    Description = "Second item of this store",
                    Name = "Batman car",
                    NumberSales = 0,
                    Price = 10000,
                    UpdateDate = null
                },
                new Product()
                {
                    Id  = 3,
                    CreationDate = DateTime.Now.AddMonths(-1),
                    Description = "Third item of this store",
                    Name = "Gauntlet of infinity",
                    NumberSales = 0,
                    Price = 10,
                    UpdateDate = null
                },
                new Product()
                {
                    Id  = 4,
                    CreationDate = DateTime.Now,
                    Description = "Fourth item of this store",
                    Name = "Iron throne",
                    NumberSales = 0,
                    Price = 2500,
                    UpdateDate = null
                },
            };
            }            
        }

        public Product Create(Product entity)
        {
            if (entity == null || entity.Id > 0)
            {
                return null; // can´t work with a null entity and if it has an ID, then it's not a new item to be store, maybe the update method should be used instead
            }

            entity.CreationDate = DateTime.Now;

            entity.Id = Repo.OrderByDescending(m => m.Id).First().Id + 1;

            Repo.Add(entity);

            return entity;
        }

        public void Delete(Product entity)
        {
            Repo.Remove(entity);
        }

        public List<Product> GetAll()
        {
            return Repo;
        }

        public Product GetById(int id)
        {
            return Repo.SingleOrDefault(m => m.Id == id); // id is unique so it's single or null
        }

        public Product Update(Product entity)
        {
            // in this class there is no EF so we have to map properties
            Product oldProduct = Repo.SingleOrDefault(m => m.Id == entity.Id); //id is unique so it's single or null

            if (oldProduct == null)
            {
                // there should be bussiness logic here to know if we create a new product or just pass an error, in this case, passing an error
                return null; // return Create(entity)
            }
            entity.UpdateDate = DateTime.Now;
            // we could just remove the old one from the list, and add the new one, but lets do it like EF

            var index = Repo.FindIndex(r => r.Id == entity.Id);

            if (index != -1)
            {
                Repo[index].Description = entity.Description;
                Repo[index].Name = entity.Name;
                Repo[index].NumberSales = entity.NumberSales;
                Repo[index].Price = entity.Price;
                Repo[index].UpdateDate = entity.UpdateDate;
            }

            return entity;
        }
    }
}
